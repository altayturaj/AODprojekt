package se.hig.project.maze;

import java.util.*;


/**
 * @author Altay Pourhosseini
 * @version 2021-01-10
 */

/**
 * F�rg representerar status av noder
 */
enum Color {
	WHITE, GREY, BLACK;
}

/**
 * Representerar labyrinten i grafisk format
 */
public class MazeA {
	// test
	private Random random;
	private MazeNode[][] maze;
	private int size;
	private int time;
	private boolean printDFS;
	private boolean printDFSShortest;
	private ArrayList<MazeNode> dfsNodes;

	private boolean stopAdding = false;

	/**
	 * Initierar en labyrint med given storlek
	 * 
	 * @param size Size of the matrix that is to be initiated
	 */
	public MazeA(int s) {
		// initialize all variables
		time = 0;
		printDFS = false;
		printDFSShortest = false;
		dfsNodes = new ArrayList<MazeNode>();

		random = new Random();
		size = s;
		// initialize maze with specified size
		maze = new MazeNode[size][size];
		for (int row = 0; row < size; row++)
			for (int col = 0; col < size; col++) {
				maze[row][col] = new MazeNode(row, col);
				if (row == 0 & col == 0)
					maze[row][col].hasNorthWall = false;
				if (row == size - 1 & col == size - 1)
					maze[row][col].hasSouthWall = false;
			}
		createMaze();
	}

	/**
	 * Anv�nder depth-first search f�r att hitta l�sning till labyrinten
	 */
	public void dfs() {
		// initialize vertices with color WHITE
		for (int row = 0; row < size; row++)
			for (int col = 0; col < size; col++)
				maze[row][col].color = Color.WHITE;
		time = 0; // initial time
		for (MazeNode[] arr : maze)
			for (MazeNode node : arr)
				if (node.color == Color.WHITE)
					dfsVisit(node);
		stopAdding = false;
	}

	/**
	 * Rekursiv metod f�r att bes�ka en nod med dfs
	 * 
	 * @param node Node that is to be visited
	 */
	private void dfsVisit(MazeNode node) {
		// keeps track of nodes that are visited through a depth-first search
		if (node.equals(new MazeNode(size - 1, size - 1))) {
			stopAdding = true;
			dfsNodes.add(node);
		}
		if (!stopAdding)
			dfsNodes.add(node);
		node.color = Color.GREY;
		time++;
		node.discoveryTime = time;
		// search all neighbors of the nodes
		for (MazeNode n : node.neighbors)
			if (n.color == Color.WHITE)
				dfsVisit(n);

		node.color = Color.BLACK;
		time++;
		node.finishingTime = time;
	}

	/**
	 * Genererar en perfekt labyrint, ingen loop, en l�sning och slumpad.
	 */
	public void createMaze() {
		Stack<MazeNode> stack = new Stack<MazeNode>();
		int totalCells = size * size;
		MazeNode currentNode = maze[0][0]; // start at upper left hand corner
		currentNode.setVisited();
		int visitedCells = 1;
		// Runs until all cells have been visited
		while (visitedCells < totalCells) {
			ArrayList<MazeNode> neighbors = getNeighbors(currentNode);
			if (!neighbors.isEmpty()) {
				// randomly choose an index
				int index = random.nextInt(neighbors.size());
				MazeNode node = neighbors.get(index);
				currentNode.knockDownWall(node);
				stack.push(currentNode);
				currentNode = node;
				currentNode.setVisited();
				visitedCells++;

			} else if (!stack.isEmpty())
				currentNode = stack.pop();
		}

	}

	/**
	 * Returnerar en arraylist av "neighbors" som har v�ggar och inte varit bes�kta
	 * 
	 * @param node The node whose neighbors are to be found
	 * @return ArrayList of valid neighbors
	 */
	public ArrayList<MazeNode> getNeighbors(MazeNode node) {
		ArrayList<MazeNode> neighbors = new ArrayList<MazeNode>();
		int row = node.row;
		int col = node.col;
		// top neighbor
		if (row - 1 >= 0 && maze[row - 1][col].allWallsIntact() && !maze[row - 1][col].wasVisited())
			neighbors.add(maze[row - 1][col]);
		// bottom neighbor
		if (row + 1 < size && maze[row + 1][col].allWallsIntact() && !maze[row + 1][col].wasVisited())
			neighbors.add(maze[row + 1][col]);
		// west neighbor
		if (col - 1 >= 0 && maze[row][col - 1].allWallsIntact() && !maze[row][col - 1].wasVisited())
			neighbors.add(maze[row][col - 1]);
		// east neighbor
		if (col + 1 < size && maze[row][col + 1].allWallsIntact() && !maze[row][col + 1].wasVisited())
			neighbors.add(maze[row][col + 1]);
		return neighbors;

	}

	/**
	 * Returnerar en string som representerar l�sning till labyrint
	 * 
	 * @return String of DFS maze solution
	 */
	public String printDFS() {
		printDFS = true;
		String s = this.toString();
		printDFS = false;
		return s;
	}

	/**
	 * Returnerar en string som representerar kortaste v�gen f�r labyrinten
	 * 
	 * @return Shortest path of maze solution using DFS
	 */
	public String printDFSShortestPath() {
		printDFSShortest = true;
		for (int x = dfsNodes.size() - 2; x > -1; x--)
			if (dfsNodes.get(x).finishingTime < dfsNodes.get(x + 1).finishingTime)
				dfsNodes.remove(x);
		String s = this.toString();
		printDFSShortest = false;
		return s;
	}

	@Override
	public String toString() {
		String[][] chars = new String[size * 2 + 1][size * 2 + 1];
		String s = "";
		for (int row = 0; row < chars.length; row++) {
			for (int col = 0; col < chars.length; col++) {
				// if there is not a node at the given location, node is null
				MazeNode node = null;
				if (row % 2 == 1 && col % 2 == 1)
					node = maze[row / 2][col / 2];
				// represents maze entrance and exit
				if ((row == 0 && col == 1) || (row == size * 2 && col == size * 2 - 1))
					chars[row][col] = "  ";
				// represents corners
				else if (chars[row][col] == null && row % 2 == 0 && col % 2 == 0)
					chars[row][col] = "+";
				// represents top and bottom row
				else if (row == 0 || row == chars.length - 1)
					chars[row][col] = "--";
				// represents leftmost and rightmost column
				else if (row % 2 == 1 && (col == 0 || col == chars[0].length - 1))
					chars[row][col] = "|";
				else if (node != null) {
					if (node.hasWestWall)
						chars[row][col - 1] = "|";
					else
						chars[row][col - 1] = " ";
					if (node.hasEastWall)
						chars[row][col + 1] = "|";
					else
						chars[row][col + 1] = " ";
					if (node.hasNorthWall)
						chars[row - 1][col] = "--";
					if (node.hasSouthWall)
						chars[row + 1][col] = "--";
					Integer t = (Integer) node.discoveryTime;
					Integer visit = node.numVisited;
					if (printDFS && t < 10 && dfsNodes.contains(node))
						chars[row][col] = t.toString() + " ";
					else if (printDFS && t >= 10 && dfsNodes.contains(node))
						chars[row][col] = t.toString() + "";
					else if (printDFSShortest && dfsNodes.contains(node))
						chars[row][col] = "##";

					else
						chars[row][col] = "  ";
				} else
					chars[row][col] = "  ";
			}
		}
		for (int row = 0; row < chars.length; row++) {
			for (int col = 0; col < chars.length; col++)
				s += chars[row][col];
			s += "\n";
		}
		return s;

	}

}