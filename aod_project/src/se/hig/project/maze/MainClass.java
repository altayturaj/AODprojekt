package se.hig.project.maze;

import java.awt.Point;

import java.util.Scanner;

//CodeGrade commit


/**
 * @author Altay Pourhosseini
 */
public class MainClass {

	public static void main(String[] args) {

		System.out.print("Set maze size: ");

		Scanner scan = new Scanner(System.in);
		int size = scan.nextInt();
		
		MazeA mazes = new MazeA(size);
		System.out.println("Original maze:\n\n" + mazes + "\n");

		// depth-first search
		mazes.dfs();
		System.out.println("Depth-first search:\n");
		System.out.println(mazes.printDFS());
		System.out.println("\n" + mazes.printDFSShortestPath());
		scan.close();

//		CreateMaze<Integer[][]> mazeGenerator = new CreateMazeB<Integer[][]>(size);
//		Object[][] maze = mazeGenerator.createTheMaze();
//		scan.close();
//		System.out.println("Maze:\n" + mazeGenerator.getTheMaze());
//
//		Point source = new Point(0, 0);
//		Point dest = new Point(size - 1, size - 1);
//
//		SolveMazeB<Integer> solver = new SolveMazeB<Integer>(size);
//		int dist = solver.correctPath((Integer[][]) maze, source, dest);
//
//		if (dist != Integer.MAX_VALUE)
//			System.out.println("Shortest way is " + dist);
//		else
//			System.out.println("Shortest way doesn't exist");
//
//		
//
	}
}
