package se.hig.project.maze;

import java.awt.Point;

/**
 * @author Altay Pourhosseini
 * @version 2021-01-09
 */

public class QueueNode {
	Point point;
	int dist;

	public QueueNode(Point point, int dist) {
		this.point = point;
		this.dist = dist;
	}
};