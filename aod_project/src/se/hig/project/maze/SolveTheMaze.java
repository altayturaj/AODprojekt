package se.hig.project.maze;

import java.awt.Point;
import java.util.List;



/**
 * @author Altay Pourhosseini
 * @version 2020-01-09
 */
public interface SolveTheMaze<T> {

	public boolean checkPoint(int x, int y);
	
	public int correctPath(T[][] t, Point src, Point dest);
	

	public void printPaths(List<List<QueueNode>> paths, T[][] t);

	public void printFastestPath(List<QueueNode> path, T[][] t);

}
