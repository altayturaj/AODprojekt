package se.hig.project.maze;

import java.awt.Point;
import java.util.List;


/**
 * @author Altay Pourhosseini
 * @version 2021-01-09
 */
public interface CreateMaze<T> {

	public T[][] createTheMaze();

	public String getTheMaze();

	public boolean checkNextPoint(Point point);

	public void addNeighborsRandom(List<Point> point);

	public List<Point> findNeighbors(Point point);

	public boolean pointInMaze(int x, int y);

	public boolean pointNotCorner(Point point, int x, int y);

	public boolean notSamePoint(Point point, int x, int y);
	
	

}